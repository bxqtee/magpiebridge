package org.jiucheng.magpiebridge.client.aio;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

/**
 * 
 * @author jiucheng
 *
 */
public class ClientWriteCompletionHandler implements CompletionHandler<Integer, ClientAttachment> {

	public void completed(Integer result, ClientAttachment attachment) {
		if (attachment.getWriteByteBuffer().hasRemaining()) {
			attachment.getClient().write(attachment.getWriteByteBuffer(), attachment, this);
			return;
		}
		attachment.writed.compareAndSet(true, false);
		
		attachment.setWriteByteBuffer(null);
		ByteBuffer readByteBuffer = ByteBuffer.allocate(13);
		attachment.getClient().read(readByteBuffer, attachment.setReadByteBuffer(readByteBuffer), attachment.getClientReadCompletionHandler());
	}

	public void failed(Throwable exc, ClientAttachment attachment) {
		System.out.println("write error");
		attachment.setFailed(true);
		attachment.writed.compareAndSet(true, false);
	}
}
