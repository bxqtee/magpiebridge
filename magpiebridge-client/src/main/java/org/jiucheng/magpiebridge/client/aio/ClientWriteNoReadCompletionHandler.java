package org.jiucheng.magpiebridge.client.aio;

import java.nio.channels.CompletionHandler;

/**
 * 
 * @author jiucheng
 *
 */
public class ClientWriteNoReadCompletionHandler implements CompletionHandler<Integer, ClientAttachment> {

	public void completed(Integer result, ClientAttachment attachment) {
		if (attachment.getWriteByteBuffer().hasRemaining()) {
			attachment.getClient().write(attachment.getWriteByteBuffer(), attachment, this);
			return;
		}
		attachment.writed.compareAndSet(true, false);
	}

	public void failed(Throwable exc, ClientAttachment attachment) {
		System.out.println("write heartbeat error");
		attachment.setFailed(true);
		attachment.writed.compareAndSet(true, false);
	}
}
