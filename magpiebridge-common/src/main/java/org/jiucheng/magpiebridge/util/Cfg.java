package org.jiucheng.magpiebridge.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * 
 * @author jiucheng
 *
 */
public class Cfg {
    
    private static Properties properties = new Properties();
    
    public static String getClientKey() {
        return properties.getProperty("client.key");
    }
    
    public static String getServerIp() {
        return properties.getProperty("server.ip");
    }
    
    public static int getServerPort() {
        return Integer.valueOf(properties.getProperty("server.port"));
    }
    
    public static String getServerMappings(String clientKey) {
    	if (clientKey != null) {
            return properties.getProperty(MessageFormat.format("server.{0}.mappings", clientKey));
    	} else {
    		return properties.getProperty("server.mappings");
    	}
    }
    
    public static void loadProperties(Class<?> clazz) {
        String path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
        System.out.println(path);
        String pre = "";
        if (path.indexOf(".jar") > -1) {
            pre = "../";
        }
        path = path.substring(0, path.lastIndexOf("/") + 1);
        String file = MessageFormat.format("{0}{1}conf/cfg.properties", path, pre);
        try {
            InputStream is = new FileInputStream(file);
            properties.load(is);
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

