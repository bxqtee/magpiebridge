package org.jiucheng.magpiebridge.server.aio.proxy;

import java.nio.channels.AsynchronousServerSocketChannel;

import org.jiucheng.magpiebridge.server.aio.ServerAttachment;

/**
 * 
 * @author jiucheng
 *
 */
public class ProxyEstablishmentAttachment {
    
    private AsynchronousServerSocketChannel proxy;
    private ServerAttachment serverAttachment;
    private String remote;
    
    public ProxyEstablishmentAttachment(AsynchronousServerSocketChannel proxy) {
        this.proxy = proxy;
    }
    
    public AsynchronousServerSocketChannel getProxy() {
        return proxy;
    }
    
    public ServerAttachment getServerAttachment() {
        return serverAttachment;
    }
    
    public ProxyEstablishmentAttachment setServerAttachment(ServerAttachment serverAttachment) {
        this.serverAttachment = serverAttachment;
        return this;
    }
    
    public ProxyEstablishmentAttachment setRemote(String remort) {
        this.remote = remort;
        return this;
    }
    
    public String getRemote() {
        return remote;
    }
}
