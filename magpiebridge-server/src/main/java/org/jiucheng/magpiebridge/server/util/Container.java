package org.jiucheng.magpiebridge.server.util;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.jiucheng.magpiebridge.server.aio.ServerAttachment;

/**
 * 
 * @author jiucheng
 *
 */
public class Container {
    // Client连接
    public static final ConcurrentMap<String, ServerAttachment> CLIENTS = new ConcurrentHashMap<String, ServerAttachment>();
    public static final ConcurrentHashMap<String, List<Object>> DOMAINS = new ConcurrentHashMap<String, List<Object>>();
    
    public static final AtomicInteger CLIENTS_ID = new AtomicInteger();
}
